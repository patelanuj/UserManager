package pro.anuj.hexagon.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Common pages controller
 * 
 * @author Anuj
 * 
 */

@Slf4j
@Controller
public class CommonPagesController {

    /**
     * Login page
     * 
     * @return login page id
     */
    @RequestMapping(value = { "/login" })
    public String getLoginPage() {
        log.debug("Calling Login page.");
        return "loginPage";
    }

    /**
     * Home Page
     * 
     * @return home page id
     */
    @RequestMapping(value = { "/index", "/" }, method = RequestMethod.GET)
    public String getHomePage() {
        log.debug("Calling home page.");
        return "homePage";
    }
}
