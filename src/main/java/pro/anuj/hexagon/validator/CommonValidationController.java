package pro.anuj.hexagon.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pro.anuj.hexagon.repository.AccountRepository;
import pro.anuj.hexagon.repository.RoleRepository;
import pro.anuj.hexagon.repository.TeamRepository;

/**
 * A general purpose controller used to validate fields on a form.
 * 
 * @author Anuj
 */
@Slf4j
@Controller
@RequestMapping("/validator/*")
public class CommonValidationController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    TeamRepository teamRepository;

    /**
     * Checking the existency of an email
     * 
     * @param email
     * @return "true" or "false"
     */
    @RequestMapping(value = "/checkemail", params = "email")
    public @ResponseBody
    String checkMail(@RequestParam String email) {
        log.debug("checking email : " + email);
        return (accountRepository.findByEmail(email) != null) ? "false" : "true";
    }

    /**
     * Checking the existency of a role name
     * 
     * @param roleName
     * @return "true" or "false"
     */
    @RequestMapping(value = "/checkrolename", params = "roleName")
    public @ResponseBody
    String checkRoleName(@RequestParam String roleName) {
        log.debug("checking role name : " + roleName);
        return (roleRepository.findByRoleName(roleName) != null) ? "false" : "true";
    }

    /**
     * Checking the existency of a team name
     * 
     * @param teamName
     * @return "true" or "false"
     */
    @RequestMapping(value = "/checkteamname", params = "teamName")
    public @ResponseBody
    String checkTeamName(@RequestParam String teamName) {
        log.debug("checking team name : " + teamName);
        return (teamRepository.findByTeamName(teamName) != null) ? "false" : "true";
    }
}
