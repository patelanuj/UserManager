package pro.anuj.hexagon.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import pro.anuj.hexagon.domain.Account;
import java.util.List;

/**
 * Account repository
 * 
 * @author Anuj
 * 
 */
public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {

    Account findByEmail(String email);

    List<Account> findByTeamsTeamName(String teamName);

}
