package pro.anuj.hexagon.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.anuj.hexagon.domain.Account;
import pro.anuj.hexagon.helper.AccountHelper;
import pro.anuj.hexagon.repository.AccountRepository;
import java.util.Collection;
import java.util.List;
import static com.google.common.collect.Lists.newArrayList;

/**
 * 
 * An implementation of Spring Security's UserDetailsService.
 * 
 * @author Maachou
 * 
 */
@Slf4j
@Service
public class AuthenticationService implements UserDetailsService {

    private AccountRepository userRepository;

    private AccountHelper accountHelper;

    @Autowired
    public AuthenticationService(AccountRepository userRepository, AccountHelper accountHelper,
            InitDataService initDataService) {
        this.userRepository = userRepository;
        this.accountHelper = accountHelper;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException, DataAccessException {
        if ((email == null) || email.trim().isEmpty()) {
            throw new UsernameNotFoundException("Email is null or empty");
        }

        log.debug("Checking  users with email: " + email);
        Account user = userRepository.findByEmail(email);

        if (user == null) {
            String errorMsg = "User with email: " + email + " could not be found";
            log.debug(errorMsg);
            throw new UsernameNotFoundException(errorMsg);
        }

        Collection<GrantedAuthority> grantedAuthorities = toGrantedAuthorities(accountHelper.getRoleNames(user));
        String password = user.getPassword();
        boolean enabled = user.getIsEnabled();
        boolean userNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean userNonLocked = true;

        return new org.springframework.security.core.userdetails.User(email, password, enabled, userNonExpired,
                credentialsNonExpired, userNonLocked, grantedAuthorities);
    }

    public static Collection<GrantedAuthority> toGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> result = newArrayList();

        for (String role : roles) {
            result.add(new SimpleGrantedAuthority(role));
        }

        return result;
    }
}
