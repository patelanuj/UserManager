package pro.anuj.hexagon.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.*;
import java.util.EnumSet;

/**
 * Created with IntelliJ IDEA. User: Anuj Date: 15/9/14, 1:04 PM Purpose :
 */

@Slf4j
public class WebAppInitializer implements WebApplicationInitializer {

    public void onStartup(ServletContext servletContext) throws ServletException {
        log.debug("Initialize the servlet now");

        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
        dispatcherContext.scan("pro.anuj.hexagon.config");
        log.debug("Done");

        servletContext.addListener(new ContextLoaderListener(dispatcherContext));
        servletContext.addListener(HttpSessionEventPublisher.class);
        servletContext.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));

        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("appServlet", new DispatcherServlet(
                dispatcherContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");

        FilterRegistration.Dynamic springFilter = servletContext.addFilter("springSecurityFilterChain",
                DelegatingFilterProxy.class);
        springFilter.addMappingForUrlPatterns(null, true, "/*");

        log.debug("Done");
    }
}