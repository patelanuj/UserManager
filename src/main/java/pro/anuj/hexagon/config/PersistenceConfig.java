package pro.anuj.hexagon.config;

import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA. User: Anuj Date: 15/9/14, 1:08 PM Purpose :
 */
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:persistence-${spring.profiles.active:dev}.properties" })
@EnableJpaRepositories(basePackages = { "pro.anuj.hexagon.repository" })
public class PersistenceConfig {

    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan("pro.anuj.hexagon.domain");
        emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        emf.setJpaProperties(hibernateProperties());
        return emf;
    }

    @Bean
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        String jdbcUrl = env.getProperty("jdbc.url");
        String databaseName = env.getProperty("jdbc.databaseName", "");
        String jdbcParameters = env.getProperty("jdbc.parameters", "");
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setJdbcUrl(jdbcUrl.concat(databaseName).concat("?").concat(jdbcParameters));
        dataSource.setUsername(env.getProperty("jdbc.username"));
        dataSource.setPassword(env.getProperty("jdbc.password"));

        /**
         * HikariCP specific. Remove if you move to other connection pooling library.
         **/
        dataSource.addDataSourceProperty("cachePrepStmts", true);
        dataSource.addDataSourceProperty("prepStmtCacheSize", 250);
        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
        dataSource.addDataSourceProperty("useServerPrepStmts", true);
        dataSource.addDataSourceProperty("initializationFailFast", true);
        dataSource.setIdleTimeout(60000L);
        dataSource.setMaxLifetime(120000L);

        return dataSource;
    }

    // @Bean public SpringLiquibase liquibase() {
    //
    // SpringLiquibase springLiquibase = new SpringLiquibase();
    // springLiquibase.setDataSource(dataSource());
    // springLiquibase.setChangeLog("classpath:db-change-log.xml");
    // if (env.getProperty("jdbc.clean.database", Boolean.class)) {
    // springLiquibase.setDropFirst(true);
    // }
    // return springLiquibase;
    // }

    @Bean
    public PlatformTransactionManager transactionManager() throws Exception {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(name = "hibernateProperties")
    public Properties hibernateProperties() {
        return new Properties() {
            private static final long serialVersionUID = 1779427567126967954L;

            {
                setProperty(AvailableSettings.DIALECT, env.getProperty(AvailableSettings.DIALECT));
                setProperty(AvailableSettings.HBM2DDL_AUTO, env.getProperty(AvailableSettings.HBM2DDL_AUTO));
                setProperty(AvailableSettings.SHOW_SQL, env.getProperty(AvailableSettings.SHOW_SQL));
                setProperty(AvailableSettings.USE_SQL_COMMENTS, env.getProperty(AvailableSettings.USE_SQL_COMMENTS));
                setProperty(AvailableSettings.HBM2DDL_IMPORT_FILES, "classpath: import.sql");
            }
        };
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        dataSourceInitializer.setDataSource(dataSource);
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
        databasePopulator.addScript(new ClassPathResource("import.sql"));
        dataSourceInitializer.setDatabasePopulator(databasePopulator);
        dataSourceInitializer.setEnabled(Boolean.parseBoolean(env.getProperty("jdbc.init.db")));
        return dataSourceInitializer;
    }

}
