package pro.anuj.hexagon.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <p>
 * Role entity class
 * </p>
 */
@Getter
@Setter
@Entity
@Table(name = "ROLE")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "roleName", nullable = false, unique = true)
    private String roleName;

    public Role() {
    }

    public Role(final String roleName) {
        this.roleName = roleName;
    }

    /**
     * GETTERS/SETTERS *
     */

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
