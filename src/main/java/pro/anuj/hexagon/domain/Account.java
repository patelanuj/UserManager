package pro.anuj.hexagon.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import static javax.persistence.CascadeType.PERSIST;

/**
 * <p>
 * Account entity class
 * </p>
 */

@Getter
@Setter
@Entity
@Table(name = "ACCOUNT")
public class Account extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = true, unique = false)
    private String firstName;
    @NotEmpty
    @Size(min = 1, max = 100)
    @Column(nullable = true, unique = false)
    private String lastName;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(nullable = false, unique = false)
    private String password;
    @NotNull
    @Size(min = 1, max = 50)
    @Column(nullable = false, unique = true)
    private String email;
    @NotNull
    @Column(nullable = true, unique = false)
    private Boolean isEnabled;
    @JoinTable(name = "ACCOUNT_ROLE", joinColumns = @JoinColumn(name = "ACCOUNT_ID"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @ManyToMany(cascade = PERSIST)
    private List<Role> roles = new ArrayList<Role>();
    @JoinTable(name = "ACCOUNT_TEAM", joinColumns = @JoinColumn(name = "ACCOUNT_ID"), inverseJoinColumns = @JoinColumn(name = "team_id"))
    @ManyToMany(cascade = PERSIST)
    private List<Team> teams = new ArrayList<Team>();

}
