package pro.anuj.hexagon.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import javax.persistence.MappedSuperclass;

/**
 * Hexagon Global IT Services (ALL RIGHTS RESERVED) Created with IntelliJ IDEA. User: anuj Date: 27/1/14, 8:06 PM
 * Purpose Served :
 */

@MappedSuperclass
public abstract class AbstractEntity {

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
