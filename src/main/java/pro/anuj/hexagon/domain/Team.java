package pro.anuj.hexagon.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <p>
 * Team entity class
 * </p>
 */
@Getter
@Setter
@Entity
@Table(name = "TEAM")
@NoArgsConstructor
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "teamName", nullable = false, unique = true)
    private String teamName;
}
